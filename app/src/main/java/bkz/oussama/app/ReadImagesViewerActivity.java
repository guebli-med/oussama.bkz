package bkz.oussama.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ReadImagesViewerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_images_viewer);

        Intent intent = getIntent();

        if(intent.getParcelableExtra("image") != null && intent.getParcelableExtra("image") instanceof CloudImage){
            CloudImage image = intent.getParcelableExtra("image");
            ArrayList<String> urls = new ArrayList<>();
            urls.add(image.get_imageUrl());

            TextView imageNameView = (TextView) findViewById(R.id.image_name);
            TextView imageTypeView = (TextView) findViewById(R.id.image_type);
            final ImageView cloudImageView = (ImageView) findViewById(R.id.cloud_image);

            imageNameView.setText("Name of the image : " + image.get_name());
            imageTypeView.setText("Type of the image : " + image.get_type());

            ReadImagesSourceAsync imagesSourceAsync = new ReadImagesSourceAsync();
            imagesSourceAsync.setContext(this);
            imagesSourceAsync.setProgressDialog(new ProgressDialog(this));
            imagesSourceAsync.setCallback(new ReadImagesCallback() {
                @Override
                public void finish(List<CloudImage> images) {}

                @Override
                public void imagesLoaded(List<Bitmap> images) {
                    if(images.size() > 0)
                        cloudImageView.setImageBitmap(images.get(0));
                }
            });
            imagesSourceAsync.execute(urls);
        }
        else{
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }
}
