package bkz.oussama.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainInterfaceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_interface);
    }


    public void readAlertImages(View v){
        Intent intent = new Intent(this, ReadImagesActivity.class);
        intent.putExtra("type", ReadImagesActivity.ALERT_IMAGE);
        startActivity(intent);
    }

    public void readPubImages(View v){
        Intent intent = new Intent(this, ReadImagesActivity.class);
        intent.putExtra("type", ReadImagesActivity.PUB_IMAGE);
        startActivity(intent);
    }

    /**
     * Pick an image and start upload image activity to send it to the cloud server
     * @param v
     */
    public void uploadImageIntoCloud(View v){
        Intent intent = new Intent(this, UploadImageActivity.class);
        startActivity(intent);
    }

    public void openAltranWebsite(View v){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.altran.com/fr/fr/"));
        startActivity(browserIntent);
    }
}
