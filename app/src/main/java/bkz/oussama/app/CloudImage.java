package bkz.oussama.app;

import android.os.Parcel;
import android.os.Parcelable;



public class CloudImage implements Parcelable{
    private String _name;
    private String _type;
    private String _imageUrl;
    private String _thumbnailUrl;

    public CloudImage(String name, String type, String imageUrl, String thumbnailUrl){
        _name = name;
        _type = type;
        _imageUrl = imageUrl;
        _thumbnailUrl = thumbnailUrl;
    }

    public String get_name() {
        return _name;
    }

    public String get_type() {
        return _type;
    }

    public String get_imageUrl() {
        return _imageUrl;
    }

    public String get_thumbnailUrl() {
        return _thumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{
                _name, _type, _imageUrl, _thumbnailUrl
        });
    }

    private CloudImage(Parcel in) {
        String[] data = new String[4];
        in.readStringArray(data);
        _name = data[0];
        _type = data[1];
        _imageUrl = data[2];
        _thumbnailUrl = data[3];
    }

    public static final Parcelable.Creator<CloudImage> CREATOR
            = new Parcelable.Creator<CloudImage>() {
        public CloudImage createFromParcel(Parcel in) {
            return new CloudImage(in);
        }

        public CloudImage[] newArray(int size) {
            return new CloudImage[size];
        }
    };
}