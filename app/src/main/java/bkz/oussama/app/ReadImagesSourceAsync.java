package bkz.oussama.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;



public class ReadImagesSourceAsync extends AsyncTask<List<String>, Void, List<Bitmap>> {
    ProgressDialog progressDialog;
    Activity ctx;
    HttpURLConnection conn;
    ReadImagesCallback callback;

    public void setProgressDialog(ProgressDialog dialog){
        progressDialog = dialog;
    }

    public void setContext(Activity a){
        ctx = a;
    }

    public void setCallback(ReadImagesCallback callback){
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("\tLoading images...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected List<Bitmap> doInBackground(List<String>... params) {
        try {
            List<Bitmap> bitmaps = new ArrayList<>();
            for(String url: params[0]){
                InputStream is = (InputStream) new URL(url).getContent();
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                bitmaps.add(bitmap);
            }
            return bitmaps;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<Bitmap> bitmaps) {
        //this method will be running on UI thread
        progressDialog.dismiss();

        if(callback != null)
            callback.imagesLoaded(bitmaps);
    }
}