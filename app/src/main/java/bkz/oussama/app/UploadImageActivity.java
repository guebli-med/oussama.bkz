package bkz.oussama.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static bkz.oussama.app.MainActivity.CONNECTION_TIMEOUT;
import static bkz.oussama.app.MainActivity.READ_TIMEOUT;

public class UploadImageActivity extends AppCompatActivity {

    public static final String UPLOAD_IMAGES_URL = "http://" + Utils.HOST + "/ouss.bkz.php/upload.php";

    ImageView selectedImageView;
    EditText imageNameView;

    String imageName = null;
    String imageExtension = null;
    String image64 = null;
    String thumbnail64 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);

        selectedImageView = (ImageView) findViewById(R.id.selected_image_view);
        imageNameView = (EditText) findViewById(R.id.image_name_view);
    }

    /**
     * Choose an image to send it to the cloud
     * @param v
     */
    public void chooseImage(View v){
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            try {
                Uri uri = data.getData();
                String path = getPath(uri);
                InputStream inputStream = getContentResolver().openInputStream(uri);

                /**
                 * Image
                 */
                ByteArrayOutputStream byteArrayImage = new ByteArrayOutputStream();
                Bitmap image = BitmapFactory.decodeStream(inputStream);
                image.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayImage);

                /**
                 * Thumbnail
                 */
                ByteArrayOutputStream byteArrayThumbnail = new ByteArrayOutputStream();
                Bitmap thumbnail = Bitmap.createScaledBitmap(image, 100, 100, false);
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayThumbnail);

                image64 = Base64.encodeToString(byteArrayImage.toByteArray(), Base64.DEFAULT);
                thumbnail64 = Base64.encodeToString(byteArrayThumbnail.toByteArray(), Base64.DEFAULT);
                imageName = new File(path).getName();
                imageExtension = imageName.substring(imageName.lastIndexOf("."));
                imageName = imageName.substring(0, imageName.lastIndexOf("."));

                selectedImageView.setImageBitmap(image);
                imageNameView.setText(imageName);
            }
            catch (IOException ex){
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }


    /**
     * Return the path of the uri
     * @param uri
     * @return
     */
    public String getPath(Uri uri){
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    /**
     * Send the selected image to the cloud
     * @param v
     */
    public void sendImage(View v){
        
        if(image64 == null || image64.equals("")){
            Toast.makeText(this, "You need to choose an image before sending it", Toast.LENGTH_SHORT).show();
            return;
        }
        
        class SendImageAsync extends AsyncTask<String, String, String>{
            ProgressDialog pdLoading = new ProgressDialog(UploadImageActivity.this);
            HttpURLConnection conn;
            URL url = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //this method will be running on UI thread
                pdLoading.setMessage("\tUploading image...");
                pdLoading.setCancelable(false);
                pdLoading.show();
            }

            protected String doInBackground(String... params) {
                try {
                    // Enter URL address where your php file resides
                    url = new URL(UploadImageActivity.UPLOAD_IMAGES_URL);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return "exception";
                }
                try {
                    // Setup HttpURLConnection class to send and receive data from php and mysql
                    conn = (HttpURLConnection)url.openConnection();
                    conn.setReadTimeout(READ_TIMEOUT);
                    conn.setConnectTimeout(CONNECTION_TIMEOUT);
                    conn.setRequestMethod("POST");

                    // setDoInput and setDoOutput method depict handling of both send and receive
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    // Append parameters to URL
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("name", params[0])
                            .appendQueryParameter("extension", params[1])
                            .appendQueryParameter("type", params[2])
                            .appendQueryParameter("image", params[3])
                            .appendQueryParameter("thumbnail", params[4]);
                    String query = builder.build().getEncodedQuery();

                    // Open connection for sending data
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    conn.connect();

                } catch (IOException e1) {
                    e1.printStackTrace();
                    return "exception";
                }

                try {

                    int response_code = conn.getResponseCode();

                    // Check if successful connection made
                    if (response_code == HttpURLConnection.HTTP_OK) {

                        // Read data sent from server
                        InputStream input = conn.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                        StringBuilder result = new StringBuilder();
                        String line;

                        while ((line = reader.readLine()) != null) {
                            result.append(line);
                        }

                        // Pass data to onPostExecute method
                        return(result.toString());

                    }else{
                        return("unsuccessful");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    return "exception";
                } finally {
                    conn.disconnect();
                }
            }

            @Override
            protected void onPostExecute(String result) {
                //this method will be running on UI thread
                Log.e("RESULT", result);

                pdLoading.dismiss();

                if(result.equalsIgnoreCase("true"))
                {
                    Toast.makeText(UploadImageActivity.this, "Your image was successfully uploaded.", Toast.LENGTH_SHORT).show();
                    UploadImageActivity.this.finish();
                }else{
                    Log.e("=======>", result);
                }
            }
        }

        imageName = imageNameView.getText().toString();

        new SendImageAsync().execute(imageName, imageExtension, "alert", image64, thumbnail64);
    }

}
