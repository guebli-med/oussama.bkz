package bkz.oussama.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import static bkz.oussama.app.MainActivity.CONNECTION_TIMEOUT;
import static bkz.oussama.app.MainActivity.READ_TIMEOUT;

public class ReadImagesInfoAsync extends AsyncTask<String, String, List<CloudImage>> {
    ProgressDialog progressDialog;
    Activity ctx;
    HttpURLConnection conn;
    URL url = null;
    ReadImagesCallback callback;

    public void setProgressDialog(ProgressDialog dialog){
        progressDialog = dialog;
    }

    public void setContext(Activity a){
        ctx = a;
    }

    public void setCallback(ReadImagesCallback callback){
        this.callback = callback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("\tGetting info...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected List<CloudImage> doInBackground(String... params) {
        try {
            // Enter URL address where your php file resides
            url = new URL(ReadImagesActivity.READ_IMAGES_URL);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
            Uri.Builder builder = new Uri.Builder().appendQueryParameter("type", params[0]);
            String query = builder.build().getEncodedQuery();

            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            conn.connect();

        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                List<CloudImage> images = new ArrayList<>();
                String json = result.toString();

                try {
                    JSONArray array = new JSONArray(json);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        CloudImage image = new CloudImage(
                                obj.getString("name"),
                                obj.getString("type"),
                                obj.getString("url"),
                                obj.getString("thumbnail")
                        );
                        images.add(image);
                    }
                    return images;
                }catch (Exception ex){
                    Log.e("JSON BLOC ERROR", ex.getMessage());
                    return null;
                }
                // Pass data to onPostExecute method

            }else{
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(List<CloudImage> images) {
        //this method will be running on UI thread
        progressDialog.dismiss();

        if(callback != null)
            callback.finish(images);
    }
}