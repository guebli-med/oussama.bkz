package bkz.oussama.app;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by guebli.med on 06/08/2017.
 */

public interface ReadImagesCallback {
    void finish(List<CloudImage> images);
    void imagesLoaded(List<Bitmap> images);
}
