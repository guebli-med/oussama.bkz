package bkz.oussama.app;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ReadImagesActivity extends ListActivity {

    public static final String READ_IMAGES_URL = "http://" + Utils.HOST + "/ouss.bkz.php/read_images.php";

    private String type = "alert";
    public static final String ALERT_IMAGE = "alert";
    public static final String PUB_IMAGE = "pub";

    TextView readImagesAlertView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_images);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        if(type.equals(ALERT_IMAGE) || type.equals(PUB_IMAGE))
            readImagesInfo();


        readImagesAlertView = (TextView) findViewById(R.id.read_images_alert);
    }

    public void readImagesInfo(){
        ReadImagesInfoAsync imageReader = new ReadImagesInfoAsync();
        imageReader.setContext(this);
        imageReader.setProgressDialog(new ProgressDialog(this));
        imageReader.setCallback(new ReadImagesCallback() {
            @Override
            public void finish(final List<CloudImage> imagesInfo) {
                if(imagesInfo != null && imagesInfo.size() > 0) {

                    ReadImagesSourceAsync imagesSourceAsync = new ReadImagesSourceAsync();
                    imagesSourceAsync.setContext(ReadImagesActivity.this);
                    imagesSourceAsync.setProgressDialog(new ProgressDialog(ReadImagesActivity.this));
                    imagesSourceAsync.setCallback(new ReadImagesCallback() {
                        @Override
                        public void finish(List<CloudImage> images) {}

                        @Override
                        public void imagesLoaded(List<Bitmap> bitmaps) {
                            ListImageAdapter adapter = new ListImageAdapter(
                                    ReadImagesActivity.this,
                                    ReadImagesActivity.this.getNames(imagesInfo),
                                    imagesInfo,
                                    bitmaps);
                            ListView list = (ListView) findViewById(android.R.id.list);
                            list.setAdapter(adapter);
                        }
                    });

                    List<String> urls = new ArrayList<>();

                    for(CloudImage image: imagesInfo){
                        urls.add(image.get_thumbnailUrl());
                        Log.e("URL", image.get_thumbnailUrl());
                    }

                    imagesSourceAsync.execute(urls);
                }
                else if(imagesInfo != null && imagesInfo.size() == 0){
                    readImagesAlertView.setText("No images were found");
                }
            }

            @Override
            public void imagesLoaded(List<Bitmap> images) {
            }
        });
        imageReader.execute(type);
    }

    public String[] getNames(List<CloudImage> images){
        String[] names = new String[images.size()];
        for (int i = 0; i < names.length; i++) {
            names[i] = images.get(i).get_name();
        }
        return names;
    }

    public class ListImageAdapter extends ArrayAdapter<String> {

        private Activity context;
        private List<CloudImage> imagesInfo;
        private List<Bitmap> imagesSource;

        public ListImageAdapter(Activity context, String[] names, List<CloudImage> imagesInfo, List<Bitmap> imagesSource) {
            super(context, R.layout.list_image, names);
            this.context = context;
            this.imagesInfo = imagesInfo;
            this.imagesSource = imagesSource;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.list_image, null,true);

            TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

            txtTitle.setText(imagesInfo.get(position).get_name());
            txtTitle.setTextColor(Color.WHITE);
            imageView.setImageBitmap(imagesSource.get(position));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ReadImagesActivity.this, ReadImagesViewerActivity.class);
                    intent.putExtra("image", imagesInfo.get(position));
                    startActivity(intent);
                }
            });

            return rowView;
        }
    }
}
