<?php


	if(isset($_POST['type'])){

		include 'db.config.php';

		$type = $_POST['type'];
		$sql = 'SELECT *FROM images WHERE type=:type';

		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':type', $type, PDO::PARAM_STR);
		$stmt->execute();

		$data = array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			array_push($data, array(
					'id' => $row['id'],
					'name' => $row['name'],
					'type' => $row['type'],
					'url' => "http://163.172.136.4:".$_SERVER['SERVER_PORT']."/ouss.bkz.php/".$row['url'],
					'thumbnail' => "http://163.172.136.4:".$_SERVER['SERVER_PORT']."/ouss.bkz.php/".$row['thumbnail'],
				));
		}

		echo json_encode($data);
	}

?>