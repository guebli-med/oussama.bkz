<?php

	if(isset($_POST['name']) && isset($_POST['extension']) && isset($_POST['type']) && isset($_POST['image']) && isset($_POST['thumbnail'])){

		$name = $_POST['name'];
		$extension = $_POST['extension'];
	    $type = $_POST['type'];
	    $image = $_POST['image'];
	    $thumbnail = $_POST['thumbnail'];

	    $fullname = $name.$extension;
	    
		$urls = insertImageInfo($fullname, $type);

		try{
			header('Content-Type: bitmap; charset=utf-8');

			$binaryImage = base64_decode($image);
			$imageFile = fopen($urls['imageUrl'], 'wb');
			fwrite($imageFile, $binaryImage);
			fclose($imageFile);

			$binaryThumbnail = base64_decode($thumbnail);
			$thumbnailFile = fopen($urls['thumbnailUrl'], 'wb');
			fwrite($thumbnailFile, $binaryThumbnail);
			fclose($thumbnailFile);

			echo "true";
		}
		catch(Exception $ex){
			echo $ex->getMessage();
			echo "false";
		}
	}


	function insertImageInfo($name, $type){
		include 'db.config.php';

		$sql = 'INSERT INTO images(id, name, type) VALUES (NULL, :name, :type);';

		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt->bindParam(':type', strtolower($type), PDO::PARAM_STR);
		$stmt->execute();

		$id = $pdo->lastInsertId();

		$imageUrl = "images/".$id."_".$name;
		$thumbnailUrl = "images/thumbnail/".$id."_".$name;

		$update = 'UPDATE `images` SET url=:url, thumbnail=:thumbnail WHERE id=:id;';
		$stmt = $pdo->prepare($update);

		$stmt->bindParam(':url', $imageUrl, PDO::PARAM_STR);
		$stmt->bindParam(':thumbnail', $thumbnailUrl, PDO::PARAM_STR);
		$stmt->bindParam(':id', $id, PDO::PARAM_STR);
		$stmt->execute();

		return array(
			'imageUrl' => $imageUrl,
			'thumbnailUrl' => $thumbnailUrl
			);
	}
?>




